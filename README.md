# stripes-kint-components
A repository to hold components designed to bridge the gap between K-Int's web toolkit based backend modules and stripes.

## Installation and usage
Simply run
```
yarn add @k-int/stripes-kint-components
```
This repository is available as a public NPM and is pre-transpiled, so it should be ready to work immediately.

In the case where components from this repository require translations, it will need to be added to the `stripesDeps` array in the implementing module's `package.json`.

## Development
In order to develop this module within a stripes workspace, a couple of steps are needed
- An environment variable must be set up (See https://chlee.co/how-to-setup-environment-variables-for-windows-mac-and-linux/), `STRIPES_TRANSPILE_TOKENS="@k-int"`
- Clone this repository `git clone git@gitlab.com:knowledge-integration/folio/stripes-kint-components.git`
- Delete all the `devDependencies` in your local package.json. (DO NOT MERGE THESE CHANGES BACK TO MASTER)
- Nuke your workspace node_modules and rebuild

### What went wrong
- If you aren't seeing changes take effect double check
  - Your environment variable is definitely set `printenv STRIPES_TRANSPILE_TOKENS` on Linux, Windows may differ
  - The `es` directory does not exist inside your local copy of kint-components. When built, this directory is constructed, and will take precedence if present, so if present, always delete for development work
- If you aren't seeing translations properly, ensure stripes-kint-components is present in the stripesDeps of your implementing module

## Documentation
The gitlab repository for this project can be found [here](https://gitlab.com/knowledge-integration/folio/stripes-kint-components).

### Quick links
  - [Components](https://gitlab.com/knowledge-integration/folio/stripes-kint-components/-/tree/main/src/lib)
  - [Hooks](https://gitlab.com/knowledge-integration/folio/stripes-kint-components/-/tree/main/src/lib/hooks)
  - [Utility functions](https://gitlab.com/knowledge-integration/folio/stripes-kint-components/-/tree/main/src/lib/utils)

## Release Matrix
Please note, the "Flower releases" does not indicate final compatibility. In theory, as a built 3rd party artifact, v1 of kint-components should still actually work with an otherwise Orchid (say) frontend.

However as a developer using kint-components locally, the compatibility will differ.

If the compatibility chart below states compatible with a certain flower release and that is not found, check copies of `@folio/stripes`, `@folio/stripes-core` in the workspace with `yarn why`. It is possible that kint-components 2.8.6, say, is compatible with ^6.0.0 of stripes-erm-components (6.0.0 -> Kiwi), which pulls in 6.2.2 on build (6.2.2 -> Morning Glory) and that that difference causes an issue on build locally which would not be present in any production system, and can be worked around by locking the troublesome dependency to the correct one for the release at hand locally.

Release major | Latest release | Flower releases present in (ERM) | Flower compatibility
--- | --- | --- | --- |
v1 | 1.7.0 | Lotus | Kiwi - Morning Glory |
v2 | 2.8.6 | Morning Glory | Kiwi - Morning Glory |
v3 | 3.2.3 | Nolana | Morning Glory - Nolana |
v4 | 4.7.0 | Orchid | Morning Glory - Orchid+ |
v5 | 5.1.3 | Poppy | Morning Glory - Poppy+ |

