// eslint-disable-next-line import/no-extraneous-dependencies
const path = require('path');
const jestConfig = require('@folio/stripes-erm-testing/jest.config');

const thisConfig = {
  ...jestConfig,
  rootDir: 'src',
  testEnvironment: 'jsdom',
  transform: { '^.+\\.(js|jsx)$': path.join(__dirname, './test/jest/jest-transformer.js') },
  moduleDirectories: ['node_modules'],
  modulePathIgnorePatterns: ["<rootDir>/.*/__mocks__"],
  setupFiles: [
    path.join(__dirname, './test/jest/setupTests.js')
  ],
  resolver: path.join(__dirname, './test/jest/resolver.js'),
};

module.exports = thisConfig;
