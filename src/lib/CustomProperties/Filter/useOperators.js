import * as CUSTPROP_TYPES from '../../constants/customProperties';
import { useKintIntl } from '../../hooks';

const {
  REFDATA_CLASS_NAME: REFDATA,
  MULTI_REFDATA_CLASS_NAME: MULTIREFDATA,
  DECIMAL_CLASS_NAME: DECIMAL,
  INTEGER_CLASS_NAME: INTEGER,
  DATE_CLASS_NAME: DATE,
  TEXT_CLASS_NAME: TEXT,
} = CUSTPROP_TYPES;

const useOperators = (type = null, passedIntlKey = null, passedIntlNS = null, labelOverrides = {}) => {
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  const operators = [
    { value: ' isSet', label: kintIntl.formatKintMessage({ id: 'operator.isSet', overrideValue: labelOverrides.operatorIsSet }), noValueAllowed: true },
    { value: ' isNotSet', label: kintIntl.formatKintMessage({ id: 'operator.isNotSet', overrideValue: labelOverrides.operatorIsNotSet }), noValueAllowed: true },
  ];

  if (!type || type === INTEGER || type === DECIMAL) {
    operators.push(
      { value: '==', label: kintIntl.formatKintMessage({ id: 'operator.equals', overrideValue: labelOverrides.operatorEquals }) },
      { value: '!=', label: kintIntl.formatKintMessage({ id: 'operator.doesNotEqual', overrideValue: labelOverrides.operatorDoesNotEqual }) },
      { value: '>=', label: kintIntl.formatKintMessage({ id: 'operator.isGreaterThanOrEqual', overrideValue: labelOverrides.operatorIsGreaterThanOrEqual }) },
      { value: '<=', label: kintIntl.formatKintMessage({ id: 'operator.isLessThanOrEqual', overrideValue: labelOverrides.operatorIsLessThanOrEqual }) },
    );
  }

  if (type === DATE) { // Basically the same as the "number" types above, so no need to reassign if there is no type
    operators.push(
      { value: '==', label: kintIntl.formatKintMessage({ id: 'operator.equals', overrideValue: labelOverrides.operatorEquals }) },
      { value: '!=', label: kintIntl.formatKintMessage({ id: 'operator.doesNotEqual', overrideValue: labelOverrides.operatorDoesNotEqual }) },
      { value: '>=', label: kintIntl.formatKintMessage({ id: 'operator.isOnOrAfter', overrideValue: labelOverrides.operatorIsOnOrAfter }) },
      { value: '<=', label: kintIntl.formatKintMessage({ id: 'operator.isOnOrBefore', overrideValue: labelOverrides.operatorIsOnOrBefore }) },
    );
  }

  if (!type || type === REFDATA) {
    operators.push(
      { value: '==', label: kintIntl.formatKintMessage({ id: 'operator.is', overrideValue: labelOverrides.operatorIs }) },
      { value: '!=', label: kintIntl.formatKintMessage({ id: 'operator.isNot', overrideValue: labelOverrides.operatorIsNot }) },
    );
  }

  if (type === MULTIREFDATA) {
    operators.push(
      { value: '==', label: kintIntl.formatKintMessage({ id: 'operator.contains', overrideValue: labelOverrides.operatorContains }) },
    );
  }

  if (!type || type === TEXT) {
    operators.push(
      { value: '=~', label: kintIntl.formatKintMessage({ id: 'operator.contains', overrideValue: labelOverrides.operatorContains }) },
      { value: '!~', label: kintIntl.formatKintMessage({ id: 'operator.doesNotContain', overrideValue: labelOverrides.operatorDoesNotContain }) },
    );
  }

  return operators;
};

export default useOperators;
