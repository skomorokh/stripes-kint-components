import React from 'react';
import PropTypes from 'prop-types';
import { uniqueId } from 'lodash';
import { Field } from 'react-final-form';

import {
  Col,
  IconButton,
  Layout,
  Row,
  Select,
  Tooltip,
} from '@folio/stripes/components';

import { required as requiredValidator } from '../../validators';

import useValueProps from './useValueProps';
import useOperators from './useOperators';
import { useKintIntl } from '../../hooks';

const propTypes = {
  ariaLabelledby: PropTypes.string.isRequired,
  clearRuleValue: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  name: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
  custPropDefinition: PropTypes.shape({
    type: PropTypes.string,
    category: PropTypes.shape({
      values: PropTypes.arrayOf(PropTypes.object),
    }),
  }),
  value: PropTypes.shape({
    operator: PropTypes.string,
    value: PropTypes.string,
  }),
};

const CustomPropertiesRule = ({
  ariaLabelledby,
  clearRuleValue,
  index,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  name,
  onDelete,
  custPropDefinition = {},
  value,
}) => {
  const operators = useOperators(custPropDefinition.type, passedIntlKey, passedIntlNS, labelOverrides);
  const valueProps = useValueProps(custPropDefinition, passedIntlKey, passedIntlNS, labelOverrides);
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  const selectedOperator = operators.find(o => o.value === value?.operator);

  return (
    <Row
      key={name}
    >
      <Col xs={2}>
        <Layout className="textCentered">
          {index === 0 ? null :
            kintIntl.formatKintMessage({
              id: 'OR',
              overrideValue: labelOverrides.OR
            })
        }
        </Layout>
      </Col>
      <Col xs={4}>
        <Field
          name={`${name}.operator`}
          validate={requiredValidator}
        >
          {({ input, meta }) => (
            <Select
              {...input}
              aria-labelledby={`${ariaLabelledby}-rule-column-header-comparator`}
              dataOptions={operators}
              error={meta?.touched && meta?.error}
              onChange={e => {
                input.onChange(e);

                const newlySelectedOperator = operators.find(o => o.value === e.target.value);
                if (newlySelectedOperator?.noValueAllowed) {
                  clearRuleValue();
                }
              }}
              placeholder=" "
              required
            />
          )}
        </Field>
      </Col>
      <Col xs={4}>
        { selectedOperator?.noValueAllowed ?
          null
          :
          <Field
            aria-labelledby={`${ariaLabelledby}-rule-column-header-value`}
            name={`${name}.value`}
            required
            validate={requiredValidator}
            {...valueProps}
          />
        }
      </Col>
      <Col xs={2}>
        { index ? (
          <Tooltip
            id={uniqueId('delete-rule-btn')}
            text={
              kintIntl.formatKintMessage({
              id: 'customProperty.removeRule',
              overrideValue: labelOverrides.removeRule
            }, { number: index + 1 })}
          >
            {({ ref, ariaIds }) => (
              <IconButton
                ref={ref}
                aria-labelledby={ariaIds.text}
                icon="trash"
                onClick={onDelete}
              />
            )}
          </Tooltip>
        ) : null}
      </Col>
    </Row>
  );
};

CustomPropertiesRule.propTypes = propTypes;

export default CustomPropertiesRule;
