import React from 'react';

import { waitFor } from '@folio/jest-config-stripes/testing-library/react';
import { MemoryRouter } from 'react-router-dom';

import { contexts, customProperties, labelOverrides } from './testResources';
import CustomPropertiesView from './CustomPropertiesView';
import { renderWithKintHarness } from '../../../../test/jest';

jest.mock('../../hooks');
jest.mock('./CustomPropertiesViewCtx', () => () => <div>CustomPropertiesViewCtx</div>);

describe('CustomPropertiesView', () => {
  let renderComponent;
  beforeEach(() => {
    renderComponent = renderWithKintHarness(
      <MemoryRouter>
        <CustomPropertiesView
          contexts={contexts}
          customProperties={customProperties}
          customPropertiesEndpoint="erm/custprops"
          id="supplementaryProperties"
          labelOverrides={labelOverrides}
        />
      </MemoryRouter>
    );
  });

  it('renders CustomPropertiesViewCtx component ', () => {
    const { getByText } = renderComponent;
     waitFor(() => expect(getByText('CustomPropertiesViewCtx')).toBeInTheDocument());
  });
});
