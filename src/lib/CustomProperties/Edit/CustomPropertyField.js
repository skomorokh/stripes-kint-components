import PropTypes from 'prop-types';

import { Field } from 'react-final-form';

import orderBy from 'lodash/orderBy';

import {
  Col,
  Datepicker,
  Row,
  Select,
  TextArea,
  TextField,
  MultiSelection
} from '@folio/stripes/components';

import * as CUSTOM_PROPERTY_TYPES from '../../constants/customProperties';

import { useKintIntl } from '../../hooks';

const CustomPropertyField = ({
  availableCustomProperties,
  customProperty,
  customPropertyType,
  customProperties,
  index,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  name,
  onChange,
  value,
  setCustomProperties
}) => {
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);
  // Set up the validator for the customProperty
  const customPropertyValidator = (fieldValue, allValues) => {
    const { note, publicNote, value: currentValue } = allValues?.customProperties?.[customProperty?.value]?.[0] ?? {};

    if ((note && !currentValue) || (publicNote && !currentValue)) {
      if (
        customProperty.type === CUSTOM_PROPERTY_TYPES.DECIMAL_CLASS_NAME ||
        customProperty.type === CUSTOM_PROPERTY_TYPES.INTEGER_CLASS_NAME
      ) {
        return (
          kintIntl.formatKintMessage({
            id: 'customProperties.errors.customPropertyNoteInvalidNumber',
            overrideValue: labelOverrides.customPropertyNoteInvalidNumberError
          })
        );
      } else {
        return (
          kintIntl.formatKintMessage({
            id: 'customProperties.errors.customPropertyNoteWithoutValue',
            overrideValue: labelOverrides.customPropertyNoteWithoutValueError
          })
        );
      }
    }

    if (customProperty.type === CUSTOM_PROPERTY_TYPES.DECIMAL_CLASS_NAME) {
      const regexp = /^-?[\d]*(\.[\d]{0,2})?$/;
      return (fieldValue && !regexp.test(fieldValue)) ?
        kintIntl.formatKintMessage({
          id: 'errors.maxTwoDecimals',
          overrideValue: labelOverrides.maxTwoDecimalsError
        }) : undefined;
    }

    if (customProperty.type === CUSTOM_PROPERTY_TYPES.INTEGER_CLASS_NAME) {
      const min = Number.MIN_SAFE_INTEGER;
      const max = Number.MAX_SAFE_INTEGER;

      return (fieldValue && (fieldValue > max || fieldValue < min)) ?
        kintIntl.formatKintMessage({
          id: 'errors.valueNotInRange',
          overrideValue: labelOverrides.valueNotInRangeError
        }, { min, max }) : undefined;
    }

    if (!customProperty.primary && !currentValue) {
      return kintIntl.formatMessage({ id: 'stripes-core.label.missingRequiredField' });
    }

    return undefined;
  };

  const getCustomProperty = (customPropertyValue) => {
    return availableCustomProperties.find(cp => cp.value === customPropertyValue);
  };

  const renderCustomPropertyName = () => {
    const unsetCustomProperties = availableCustomProperties.filter(t => {
      // Only optional properties can be set
      if (t.primary) return false;

      const custPropValue = value[t.value];

      // The customProperty is unset and has no value.
      if (custPropValue === undefined) return true;

      // The customProperty is set but is marked for deletion. Allow reuse.
      if (custPropValue[0]?._delete) return true;
      return false;
    });

    const customPropertyValue = value[customProperty.value];
    const id = customPropertyValue?.[0]?.id;

    return (
      <Select
        autoFocus={!id}
        dataOptions={[customProperty, ...unsetCustomProperties]} // The selected customProperty, and the available unset customProperties
        id={`edit-customproperty-${index}-name`}
        label={
          kintIntl.formatKintMessage({
            id: 'customProperties.name',
            overrideValue: labelOverrides.name
          })
        }
        onChange={e => {
          const newValue = e.target.value;

          // Update `state.customProperties` which controls what customProperties are being edited.
          const newCustomProperties = [...customProperties];
          newCustomProperties[index] = getCustomProperty(newValue);
          setCustomProperties(newCustomProperties);

          // Update final-form (which tracks what the values for a given customProperty are) because
          // in essence we're deleting a customProperty and creating a new customProperty.
          // We do this by 1) marking the current customProperty for deletion and 2) initing
          // the new customProperty to an empty object.
          const currentValue = value[customProperty.value] ? value[customProperty.value][0] : {};
          onChange({
            ...value,
            [customProperty.value]: [{
              id: currentValue.id,
              _delete: true,
            }],
            [newValue]: [{}],
          });
        }}
        required
        value={customProperty.value}
      />
    );
  };

  const renderCustomPropertyValue = () => {
    const currentValue = value[customProperty.value] ? value[customProperty.value][0] : {};
    const min = Number.MIN_SAFE_INTEGER;
    const max = Number.MAX_SAFE_INTEGER;

    // Default change handler
    let handleChange = e => {
      onChange({
        ...value,
        [customProperty.value]: [{
          ...currentValue,
          _delete: e.target.value === '', // Delete customProperty if removing the value.
          value: e.target.value // send down the number for integers and decimal types
        }],
      });
    };
    // Figure out which component we're rendering and specify its unique props.
    let fieldProps;
    switch (customProperty.type) {
      case CUSTOM_PROPERTY_TYPES.REFDATA_CLASS_NAME:
        fieldProps = {
          component: Select,
          // don't order dataOptions here, notSet should always be first in list
          dataOptions: customProperty.options,
          format: (v) => v?.value,
        };
        break;
      case CUSTOM_PROPERTY_TYPES.MULTI_REFDATA_CLASS_NAME:
        fieldProps = {
          component: MultiSelection,
          // The "not set" value is not relevant for a multi select
          dataOptions: orderBy(customProperty.options?.filter(opt => !!opt?.value), 'label'),
          renderToOverlay: true
        };
        // MultiSelection passes the changed array as a value not an event
        handleChange = refdataArray => {
          onChange({
            ...value,
            [customProperty.value]: [{
              ...currentValue,
              _delete: refdataArray?.length === 0 ? true : undefined, // Delete customProperty if removing all values.
              value: refdataArray
            }]
          });
        };
        break;
      case CUSTOM_PROPERTY_TYPES.INTEGER_CLASS_NAME:
      case CUSTOM_PROPERTY_TYPES.DECIMAL_CLASS_NAME:
        fieldProps = {
          component: TextField,
          max,
          min,
          step: 'any',
          type: 'number',
        };
        handleChange = e => {
          onChange({
            ...value,
            [customProperty.value]: [{
              ...currentValue,
              _delete: e.target.value === '' ? true : undefined, // Delete customProperty if removing the value.
              value: parseFloat(e.target.value)
            }],
          });
        };
        break;
      case CUSTOM_PROPERTY_TYPES.TEXT_CLASS_NAME:
        fieldProps = {
          component: TextArea,
          parse: v => v  // Lets us send an empty string instead of `undefined`
        };
        break;
      case CUSTOM_PROPERTY_TYPES.DATE_CLASS_NAME:
          fieldProps = {
            component: Datepicker,
            backendDateStandard: 'YYYY-MM-DD',
            timeZone: 'UTC',
            usePortal: true
          };
          break;
      default:
        fieldProps = { component: TextField };
        break;
    }

    return (
      <Field
        data-test-customproperty-value
        id={`edit-customproperty-${index}-value`}
        label={
          kintIntl.formatKintMessage({
            id: 'customProperties.value',
            overrideValue: labelOverrides.value
          })
        }
        name={`${name}.${customProperty.value}[0].value`}
        onChange={handleChange}
        required={!customProperty.primary}
        validate={(fieldValue, allValues) => customPropertyValidator(fieldValue, allValues)}
        {...fieldProps}
      />
    );
  };

  const renderCustomPropertyVisibility = () => {
    const customPropertyObject = value[customProperty.value]?.[0] ?? {};

    const handleChange = e => {
      onChange({
        ...value,
        [customProperty.value]: [{
          ...customPropertyObject,
          internal: e.target.value
        }],
      });
    };

    return (
      <Select
        data-test-customproperty-visibility
        dataOptions={[
          {
            value: true,
            label: kintIntl.formatKintMessage({
              id: 'customProperties.internalTrue',
              overrideValue: labelOverrides.internalTrue
            })
          },
          {
            value: false,
            label: kintIntl.formatKintMessage({
              id: 'customProperties.internalFalse',
              overrideValue: labelOverrides.internalFalse
            })
          }
        ]}
        id={`edit-customproperty-${index}-visibility`}
        label={
          kintIntl.formatKintMessage({
            id: 'customProperties.visibility',
            overrideValue: labelOverrides.visibility
          })
        }
        onChange={handleChange}
        value={customPropertyObject?.internal ?? customProperty.defaultInternal}
      />
    );
  };

  const renderCustomPropertyNoteInternal = () => {
    const customPropertyObject = value[customProperty.value]?.[0] ?? {};

    const handleChange = e => {
      onChange({
        ...value,
        [customProperty.value]: [{
          ...customPropertyObject,
          note: e.target.value
        }],
      });
    };

    return (
      <TextArea
        data-test-customproperty-note
        id={`edit-customproperty-${index}-internal-note`}
        label={
          kintIntl.formatKintMessage({
            id: 'customProperties.internalNote',
            overrideValue: labelOverrides.internalNote
          })
        }
        onChange={handleChange}
        value={customPropertyObject?.note}
      />
    );
  };

  const renderCustomPropertyNotePublic = () => {
    const customPropertyObject = value[customProperty.value]?.[0] ?? {};

    const handleChange = e => {
      onChange({
        ...value,
        [customProperty.value]: [{
          ...customPropertyObject,
          publicNote: e.target.value
        }],
      });
    };

    return (
      <TextArea
        data-test-customproperty-public-note
        id={`edit-customproperty-${index}-public-note`}
        label={
          kintIntl.formatKintMessage({
            id: 'customProperties.publicNote',
            overrideValue: labelOverrides.publicNote
          })
        }
        onChange={handleChange}
        value={customPropertyObject?.publicNote}
      />
    );
  };

  return (
    <>
      <div
        data-testid="customPropertyField"
      >
        {
        customPropertyType === 'optional' &&
        <Row>
          <Col xs={12}>
            {renderCustomPropertyName()}
          </Col>
        </Row>
      }
        <Row>
          <Col md={6} xs={12}>
            {renderCustomPropertyValue()}
          </Col>
          <Col md={6} xs={12}>
            {renderCustomPropertyNoteInternal()}
          </Col>
        </Row>
        <Row>
          <Col md={6} xs={12}>
            {renderCustomPropertyVisibility()}
          </Col>
          <Col md={6} xs={12}>
            {renderCustomPropertyNotePublic()}
          </Col>
        </Row>
      </div>
    </>
  );
};

CustomPropertyField.propTypes = {
  availableCustomProperties: PropTypes.arrayOf(PropTypes.object),
  customProperty: PropTypes.object,
  customPropertyType: PropTypes.string,
  customProperties: PropTypes.arrayOf(PropTypes.object),
  index: PropTypes.number,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  name: PropTypes.string,
  onChange: PropTypes.func,
  setCustomProperties: PropTypes.func,
  value: PropTypes.object
};

export default CustomPropertyField;
