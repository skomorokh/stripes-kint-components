import React from 'react';


import CustomPropertyFormCard from './CustomPropertyFormCard';

import customProperties, { availableCustomProperties } from '../../../../test/jest/customProperties';
import { renderWithKintHarness } from '../../../../test/jest';

jest.mock('../../hooks');
jest.mock('./CustomPropertyField', () => () => <div>CustomPropertyField</div>);

const customProperty = availableCustomProperties?.find(cp => cp.value === 'AuthorIdentification');

const onChange = jest.fn();
const setCustomProperties = jest.fn();

describe('CustomPropertyFormCard', () => {
    let renderComponent;
    beforeEach(() => {
      renderComponent = renderWithKintHarness(
        <CustomPropertyFormCard
          availableCustomProperties={availableCustomProperties}
          customProperties={customProperties}
          customProperty={customProperty}
          customPropertyType="primary"
          index={0}
          internalPropertyCounter={1}
          name="customProperties"
          onChange={onChange}
          setCustomProperties={setCustomProperties}
          value={{}}
        />
      );
    });

    it('displays Author Identification card heading  ', () => {
      const { getByText } = renderComponent;
      expect(getByText('Author Identification')).toBeInTheDocument();
    });

    it('renders CustomPropertyField component ', () => {
      const { getByText } = renderComponent;
      expect(getByText('CustomPropertyField')).toBeInTheDocument();
    });

    it('renders CustomPropertyFormCard by its test ID ', () => {
      const { getByTestId } = renderComponent;
      expect(getByTestId('custom-property-form-card')).toBeInTheDocument();
    });
  });
