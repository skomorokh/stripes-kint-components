import PropTypes from 'prop-types';
import { Accordion } from '@folio/stripes/components';

import CustomPropertiesListField from './CustomPropertiesListField';
import { useCustomProperties, useKintIntl } from '../../hooks';

const CustomPropertiesEditCtx = ({
  ctx,
  customPropertiesEndpoint,
  id,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  nameOverride
}) => {
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  // Deal with all the possible label override options
  const getAccordionLabel = () => {
    // Special case for null context
    if (ctx === 'isNull') {
      return (
        kintIntl.formatKintMessage({
          id: 'customProperties',
          overrideValue: labelOverrides.noContext
        })
      );
    }

    // Chain formatKintMessages together using provided fallbackMessage to
    // allow for "If override or translation exists, use it, else use default"
    return (
      kintIntl.formatKintMessage({
        id: `customProperties.ctx.${ctx}`,
        overrideValue: labelOverrides[ctx],
        fallbackMessage: kintIntl.formatKintMessage({
          id: 'customProperties.defaultTitle',
          overrideValue: labelOverrides.defaultTitle
        }, { ctx })
      }, { ctx })
    );
  };

  const { data: custprops, isLoading } = useCustomProperties({
    ctx,
    endpoint: customPropertiesEndpoint,
    returnQueryObject: true,
    options: {
      filters: [{
        path: 'retired',
        comparator: '!=',
        value: 'true'
      }]
    }
  });

  if (isLoading) {
    return null;
  }

  return (
    custprops.length > 0 &&
    <Accordion
      id={`${id}-accordion-${ctx}`}
      label={getAccordionLabel()}
    >
      <CustomPropertiesListField
        ctx={ctx}
        customProperties={custprops}
        intlKey={passedIntlKey}
        intlNS={passedIntlNS}
        labelOverrides={labelOverrides}
        name={nameOverride ?? 'customProperties'}
      />
    </Accordion>
  );
};

CustomPropertiesEditCtx.propTypes = {
  ctx: PropTypes.string,
  customPropertiesEndpoint: PropTypes.string,
  id: PropTypes.string,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  nameOverride: PropTypes.string
};

export default CustomPropertiesEditCtx;
