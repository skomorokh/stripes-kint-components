import { waitFor } from '@folio/jest-config-stripes/testing-library/react';

import {
  Datepicker,
  MultiSelect,
  TextArea,
  TextInput,
  TestForm,
  Select
} from '@folio/stripes-erm-testing';

import CustomPropertyField from './CustomPropertyField';

import { availableCustomProperties, customProperties, renderWithKintHarness } from '../../../../test/jest';

import { value, initialValues } from './testResources';

const onSubmit = jest.fn();
jest.mock('../../hooks');

const onChange = () => {};
const setCustomProperties = () => {};

const authIdentCustom = availableCustomProperties?.find(cp => cp.value === 'AuthorIdentification');
const decimalCustProp = availableCustomProperties?.find(cp => cp.value === 'decimal');
const integerCustProp = availableCustomProperties?.find(cp => cp.value === 'integer');
const dateCustProp = availableCustomProperties?.find(cp => cp.value === 'date');
const multiRefdataCustProp = availableCustomProperties?.find(cp => cp.value === 'MultiRefdata');

const defaultProps = {
  availableCustomProperties,
  customProperties,
  customPropertyType: 'primary',
  internalPropertyCounter: 1,
  name: 'customProperties',
  onChange,
  setCustomProperties,
  value
};

describe('CustomPropertyField', () => {
  let renderComponent;
  describe('CustomPropertyField', () => {
    beforeEach(() => {
      renderComponent = renderWithKintHarness(
        <TestForm
          initialValues={initialValues}
          onSubmit={onSubmit}
        >
          <CustomPropertyField
            {...defaultProps}
            customProperty={authIdentCustom}
            index={0}
          />
        </TestForm>
      );
      const { getByTestId } = renderComponent;
      expect(getByTestId('customPropertyField')).toBeInTheDocument();
    });


    it('Displays expected value(s) label ', () => {
      const { getByText } = renderComponent;
      expect(getByText('Value(s)')).toBeInTheDocument();
    });

    test('Displays expected Value(s) dropdown values  ', () => {
      MultiSelect('Value(s)').select(['Not set', 'ORCID', 'Ringgold ID', 'Email Domain', 'Over IP Range', 'FOR ID', 'Other', 'Over Institute']);
    });

    it('Displays expected Internal note label ', () => {
      const { getByText } = renderComponent;
      expect(getByText('Internal note')).toBeInTheDocument();
    });

    test('Displays expected internal note value', async () => {
      await TextArea('Internal note').has({ value: 'this is an internal note' });
    });

    test('Displays expected visibilities dropdown values  ', async () => {
      await Select('Visibility').choose('Internal');
      await Select('Visibility').choose('Public');
    });

    it('Displays expected Public note label ', () => {
      const { getByText } = renderComponent;
      expect(getByText('Public note')).toBeInTheDocument();
    });

    test('Displays expected public note value', async () => {
      await TextArea('Public note').has({ value: 'this is a public note' });
    });
  });

  describe('Decimal field', () => {
    beforeEach(() => {
      renderComponent = renderWithKintHarness(
        <TestForm
          initialValues={initialValues}
          onSubmit={onSubmit}
        >
          <CustomPropertyField
            {...defaultProps}
            customProperty={decimalCustProp}
            index={2}
          />
        </TestForm>
      );
    });

    test('Displays expected Decimal field', async () => {
      await TextInput({ id: 'edit-customproperty-2-value' }).exists();
    });
  });

  describe('Integer field', () => {
    beforeEach(() => {
      renderComponent = renderWithKintHarness(
        <TestForm
          initialValues={initialValues}
          onSubmit={onSubmit}
        >
          <CustomPropertyField
            {...defaultProps}
            customProperty={integerCustProp}
            index={3}
          />
        </TestForm>
      );
    });

    test('Displays expected integer Field', async () => {
      await TextInput({ id: 'edit-customproperty-3-value' }).has({ value: '1' });
    });
  });

  describe('Refdata multi-select field', () => {
    beforeEach(() => {
      renderComponent = renderWithKintHarness(
        <TestForm
          initialValues={initialValues}
          onSubmit={onSubmit}
        >
          <CustomPropertyField
            {...defaultProps}
            customProperty={multiRefdataCustProp}
            index={4}
          />
        </TestForm>
      );
    });

    test('Displays expected multi-select Refdata Field', async () => {
      await MultiSelect('Value(s)').exists();
    });

    it('Displays expected value(s) label ', () => {
      const { getByText } = renderComponent;
      expect(getByText('Value(s)')).toBeInTheDocument();
    });

    test('renders multi-select Refdata values', async () => {
      await waitFor(async () => {
        await MultiSelect('Value(s)').select(['Future', 'Current', 'Historical', 'Does not apply']);
      });
    });
  });

  describe('Date field', () => {
    beforeEach(() => {
      renderComponent = renderWithKintHarness(
        <TestForm
          initialValues={initialValues}
          onSubmit={onSubmit}
        >
          <CustomPropertyField
            {...defaultProps}
            customProperty={dateCustProp}
            index={5}
          />
        </TestForm>
      );
    });

    test('renders values DatePicker', async () => {
      await Datepicker('Value(s)').exists();
    });

    test('Displays expected date Field', async () => {
      await TextInput({ id: 'edit-customproperty-5-value' }).exists();
    });
  });
});
