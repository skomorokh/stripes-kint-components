import { customProperties, refdata } from '../../../../test/jest';

const agreementStatusRefdata = refdata?.find(rdc => rdc.desc === 'SubscriptionAgreement.AgreementStatus');
const licenseAmendmentStatusRefdata = refdata?.find(rdc => rdc.desc === 'LicenseAmendmentStatus.Status');

const value = {
  AuthorIdentification: [{
    id: 2,
    internal: true,
    publicNote: 'this is a public note',
    note: 'this is an internal note',
    value: agreementStatusRefdata?.values?.find(rdv => rdv.value === 'active'),
    type: customProperties?.find(cp => cp.name === 'AuthorIdentification')
  }],
  Test: [{
    id: 4,
    internal: true,
    value: agreementStatusRefdata?.values?.find(rdv => rdv.value === 'active'),
    type: customProperties?.find(cp => cp.name === 'Test')
  }],
  decimal: [{
    id: 8,
    publicNote: 'decimal',
    note: 'decimal',
    internal: true,
    value: 1,
    type: customProperties?.find(cp => cp.name === 'decimal')
  }],
  integer: [{
    id: 6,
    publicNote: 'Integer',
    note: 'Integer',
    internal: true,
    value: 1,
    type: customProperties?.find(cp => cp.name === 'integer')
  }],
  text: [{
    id: 7,
    publicNote: 'text',
    note: 'text',
    internal: true,
    value: 'text',
    type: customProperties?.find(cp => cp.name === 'text')
  }],
  Refdata: [{
    id: 5,
    publicNote: 'refdata',
    note: 'refdata',
    internal: true,
    value: licenseAmendmentStatusRefdata?.values?.find(rdv => rdv.value === 'current'),
    type: customProperties?.find(cp => cp.name === 'Refdata')
  }]
};

const initialValues = {
  customProperties: value
};

export {
  value,
  initialValues
};
