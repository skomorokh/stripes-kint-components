import React from 'react';

import { waitFor } from '@folio/jest-config-stripes/testing-library/react';

import { MemoryRouter } from 'react-router-dom';

import CustomPropertiesEditCtx from './CustomPropertiesEditCtx';
import { renderWithKintHarness } from '../../../../test/jest';

jest.mock('./CustomPropertiesListField', () => () => <div>CustomPropertiesListField</div>);
jest.mock('../../hooks');

describe('CustomPropertiesEditCtx', () => {
  let renderComponent;
  beforeEach(() => {
    renderComponent = renderWithKintHarness(
      <MemoryRouter>
        <CustomPropertiesEditCtx
          ctx="OpenAccess"
          customPropertiesEndpoint="erm/custprops"
          id="supplementaryProperties"
        />
      </MemoryRouter>
    );
  });

  it('renders CustomPropertiesListField component ', () => {
    const { getAllByText } = renderComponent;
     waitFor(() => expect(getAllByText('CustomPropertiesListField')).toBeInTheDocument());
  });
});
