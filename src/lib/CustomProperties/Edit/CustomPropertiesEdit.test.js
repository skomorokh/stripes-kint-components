import React from 'react';

import { waitFor } from '@folio/jest-config-stripes/testing-library/react';

import { MemoryRouter } from 'react-router-dom';

import CustomPropertiesEdit from './CustomPropertiesEdit';
import { renderWithKintHarness } from '../../../../test/jest';

jest.mock('./CustomPropertiesEditCtx', () => () => <div>CustomPropertiesEditCtx</div>);

describe('CustomPropertiesEdit', () => {
  let renderComponent;
  beforeEach(() => {
    renderComponent = renderWithKintHarness(
      <MemoryRouter>
        <CustomPropertiesEdit
          contexts={[
            'isNull',
            'OpenAccess'
          ]}
          customPropertiesEndpoint="erm/custprops"
          id="supplementaryProperties"
        />
      </MemoryRouter>
    );
  });

  it('renders CustomPropertiesEditCtx component ', () => {
    const { getAllByText } = renderComponent;
     waitFor(() => expect(getAllByText('CustomPropertiesEditCtx')).toBeInTheDocument());
  });
});
