import React from 'react';

import { TestForm, TextArea, TextField } from '@folio/stripes-erm-testing';

import refdata from '../../../../test/jest/refdata';

import CustomPropertyForm from './CustomPropertyForm';

import customProperties from '../../../../test/jest/customProperties';
import { renderWithKintHarness } from '../../../../test/jest';

const initialValues = {
  ...customProperties?.find(cp => cp.name === 'test'),
  // Just massage the type/ctx value to be of the shape the form expects
  type: 'Decimal',
  ctx: [
    {
      value: 'OpenAccess',
      label: 'OpenAccess'
    }
  ],
};

const onSubmit = jest.fn();

jest.mock('../../hooks');

describe('CustomPropertyForm', () => {
  let renderComponent;
  beforeEach(() => {
    renderComponent = renderWithKintHarness(
      <TestForm
        initialValues={initialValues}
        onSubmit={onSubmit}
      >
        <CustomPropertyForm
          contextFilterOptions={[
            {
              'value': '',
              'label': 'All'
            },
            {
              'value': 'OpenAccess',
              'label': 'OpenAccess'
            },
            {
              'value': 'test',
              'label': 'test'
            },
            {
              'value': 'isNull',
              'label': 'None'
            }
          ]}
          refdata={refdata.map(rdc => ({ label: rdc.desc, value: rdc.id }))}
        />
      </TestForm>
    );
  });

  it('Displays expected label ', () => {
    const { getByText } = renderComponent;
    expect(getByText('Label')).toBeInTheDocument();
  });

  it('Displays expected name label', () => {
    const { getByText } = renderComponent;
    expect(getByText('Name')).toBeInTheDocument();
  });

  it('Displays expected description label', () => {
    const { getByText } = renderComponent;
    expect(getByText('Description')).toBeInTheDocument();
  });

  it('Displays expected context label ', () => {
    const { getByText } = renderComponent;
    expect(getByText('Context')).toBeInTheDocument();
  });

  it('Displays expected order weight label ', () => {
    const { getByText } = renderComponent;
    expect(getByText('Order weight')).toBeInTheDocument();
  });

  it('Displays expectred primary label ', () => {
    const { getByText } = renderComponent;
    expect(getByText('Primary')).toBeInTheDocument();
  });

  it('Displays expected default visibility label', () => {
    const { getByText } = renderComponent;
    expect(getByText('Default visibility')).toBeInTheDocument();
  });

  it('Displays expected type label ', () => {
    const { getByText } = renderComponent;
    expect(getByText('Type')).toBeInTheDocument();
  });

  test('Displays expected description value', async () => {
    await TextArea({ id: 'custom-prop-description' }).has({ value: 'Test description' });
  });

  test('renders the Name TextField', async () => {
    await TextField({ id: 'custom-prop-name' }).exists();
  });

  test('renders the Lable TextField', async () => {
    await TextField({ id: 'custom-prop-label' }).exists();
  });

  test('renders the order weight TextField', async () => {
    await TextField({ id: 'custom-prop-weight' }).exists();
  });

  test('renders the expected value in the Retired dropdown', () => {
    const { getByRole } = renderComponent;
    expect(getByRole('combobox', { name: 'Retired' })).toHaveDisplayValue('No');
  });

  test('renders the expected value in the Default visibility dropdown', () => {
    const { getByRole } = renderComponent;
    expect(getByRole('combobox', { name: 'Default visibility' })).toHaveDisplayValue('Internal');
  });

  test('renders the expected value in the Primary dropdown', () => {
    const { getByRole } = renderComponent;
    expect(getByRole('combobox', { name: 'Primary' })).toHaveDisplayValue('Yes');
  });

  [
    'Decimal',
    'Refdata (multi-select)',
    'Refdata',
    'Text',
    'Integer'
  ].forEach((type) => (
    it(`Displays expected ${type} option`, () => {
      const { getByText } = renderComponent;
      expect(getByText(type)).toBeInTheDocument();
    })
  ));
});
