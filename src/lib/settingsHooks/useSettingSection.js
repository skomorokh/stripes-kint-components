import { useMutation, useQuery } from 'react-query';
import { useOkapiKy } from '@folio/stripes/core';
import { generateKiwtQueryParams } from '../utils';

const useSettingSection = ({
  sectionName,
  settingEndpoint
}) => {
  const ky = useOkapiKy();
  const queryParams = generateKiwtQueryParams({
    filters: [
      {
        path: 'section',
        value: sectionName
      }
    ],
    sort: [
      {
        path: 'key'
      }
    ],
    perPage: 100,
    stats: false
  }, {});

  const { data: settings = [] } = useQuery(
    ['stripes-kint-components', 'useSetting', 'appSettings', queryParams, sectionName],
    () => ky(`${settingEndpoint}?${queryParams?.join('&')}`).json()
  );

  const { mutateAsync: putSetting } = useMutation(
    ['stripes-kint-components', 'useSetting', 'putSetting', sectionName],
    (data) => ky.put(`${settingEndpoint}${data.id ? '/' + data.id : ''}`, { json: data })
  );

  return ({
    handleSubmit: putSetting,
    settings
  });
};

export default useSettingSection;
