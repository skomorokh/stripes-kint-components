import { useQuery } from 'react-query';
import { useOkapiKy } from '@folio/stripes/core';
import { generateKiwtQueryParams } from '../utils';

const useAppSettings = ({
  endpoint,
  keyName,
  options,
  returnQueryObject = false,
  sectionName,
  queryParams
}) => {
  const ky = useOkapiKy();

  const defaultQueryParamObject = {
    perPage: 100,
    stats: false,
  };

  const namespaceArray = [endpoint];

  if (sectionName) {
    defaultQueryParamObject.filters = [
      ...(defaultQueryParamObject.filters ?? []),
      {
        path: 'section',
        value: sectionName
      }
    ];

    namespaceArray.push(sectionName);
  }

  if (keyName) {
    defaultQueryParamObject.filters = [
      ...(defaultQueryParamObject.filters ?? []),
      {
        path: 'key',
        value: keyName
      }
    ];

    defaultQueryParamObject.perPage = 1;

    namespaceArray.push(keyName);
  }

  const queryParamArray = generateKiwtQueryParams(options ?? defaultQueryParamObject, {});
  namespaceArray.push(queryParamArray);

  const queryObject = useQuery(
    namespaceArray,
    () => ky(`${endpoint}?${queryParamArray?.join('&')}`).json(),
    queryParams
  );


  if (returnQueryObject) {
    return queryObject;
  }

  if ((queryParams ?? defaultQueryParamObject)?.perPage && (queryParams ?? defaultQueryParamObject)?.perPage === 1) {
    return queryObject?.data?.[0] ?? {};
  }

  return queryObject?.data ?? [];
};

export default useAppSettings;
