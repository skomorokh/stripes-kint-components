import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Button, Icon } from '@folio/stripes/components';
import css from '../../../styles/NoResultsMessage.css';
import { useKintIntl } from '../hooks';

const NoResultsMessage = ({
  icon: userIcon,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  isLoading,
  isError,
  error,
  filterPaneIsVisible,
  label: userLabel,
  labelOverrides = {},
  searchTerm,
  toggleFilterPane,
}) => {
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  let icon = 'search';
  let label = <FormattedMessage id="stripes-smart-components.sas.noResults.default" values={{ searchTerm }} />;

  // No search term but no results
  if (!searchTerm) {
    icon = 'search';
    label = <FormattedMessage id="stripes-smart-components.sas.noResults.noResults" />;
  }

  // Loading results
  if (isLoading) {
    icon = 'spinner-ellipsis';
    label = <FormattedMessage id="stripes-smart-components.sas.noResults.loading" />;
  }

  // Request failure
  if (isError) {
    icon = 'exclamation-circle';
    label = error?.message;
  }

  return (
    <div className={css.noResultsMessage}>
      <div className={css.noResultsMessageLabelWrap}>
        {(icon || userIcon) &&
          <Icon
            icon={userIcon ?? icon}
            iconRootClass={css.noResultsMessageIcon}
          />
        }
        <span className={css.noResultsMessageLabel}>{userLabel ?? label}</span>
      </div>
      {/* If the filter pane is closed we show a button that toggles filter pane */}
      {!filterPaneIsVisible && (
        <Button
          buttonClass={css.noResultsMessageButton}
          marginBottom0
          onClick={toggleFilterPane}
        >
          {
            kintIntl.formatKintMessage({
              id: 'showFilters',
              overrideValue: labelOverrides?.showFilters
            })
          }
        </Button>
      )}
    </div>
  );
};

NoResultsMessage.propTypes = {
  error: PropTypes.object,
  filterPaneIsVisible: PropTypes.bool.isRequired,
  icon: PropTypes.string,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  isLoading: PropTypes.bool,
  isError: PropTypes.bool,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.func
  ]),
  labelOverrides: PropTypes.object,
  searchTerm: PropTypes.string,
  toggleFilterPane: PropTypes.func.isRequired,
};

export default NoResultsMessage;
