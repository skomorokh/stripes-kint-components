import React from 'react';
import PropTypes from 'prop-types';

import { Field } from 'react-final-form';
import {
  Select,
  TextField,
} from '@folio/stripes/components';
import RefdataButtons from '../../RefdataButtons';
import { useKintIntl } from '../../hooks';

const EditSettingValue = (props) => {
  const {
    currentSetting: setting,
    input,
    intlKey: passedIntlKey,
    intlNS: passedIntlNS,
    labelOverrides = {},
    refdata,
    templates
  } = props;

  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  const fieldLabel = kintIntl.formatKintMessage({
    id: 'settings.valueFor',
    overrideValue: labelOverrides?.valueFor
  }, { name: setting.key });

  switch (setting.settingType) {
    case 'Refdata':
      // Grab refdata values corresponding to setting
      // eslint-disable-next-line no-case-declarations
      let RefdataComponent = Select;
      if (refdata.length > 0 && refdata.length <= 4) {
        RefdataComponent = RefdataButtons;
      }

      // Adding default sort to refdata object in ascending order by label
      const sortByLabel = (a, b) => (a.label.localeCompare(b.label));
      const sortedRefdata = refdata.sort(sortByLabel);

      return (
        <Field
          aria-label={fieldLabel}
          component={RefdataComponent}
          dataOptions={sortedRefdata}
          name={`${input.name}.value`}
        />
      );

    case 'Password':
      return (
        <Field
          aria-label={fieldLabel}
          autoFocus
          component={TextField}
          name={`${input.name}.value`}
          parse={v => v} // Lets us send an empty string instead of 'undefined'
          type="password"
        />
      );
    case 'Template': {
      // Grab refdata values corresponding to setting
      // eslint-disable-next-line no-case-declarations
      const selectTemplateValues = [
        { value: '', label: '' },
        ...templates.map(t => ({ value: t.id, label: t.name }))
      ];

      return (
        <Field
          aria-label={fieldLabel}
          component={Select}
          dataOptions={selectTemplateValues}
          name={`${input.name}.value`}
          parse={v => v}
        />
      );
    }
    default:
      // If in doubt, go with String
      return (
        <Field
          aria-label={fieldLabel}
          autoFocus
          component={TextField}
          name={`${input.name}.value`}
          parse={v => v} // Lets us send an empty string instead of 'undefined'
        />
      );
  }
};

EditSettingValue.propTypes = {
  currentSetting: PropTypes.shape({
    settingType: PropTypes.string,
    key: PropTypes.string,
  }),
  input: PropTypes.shape({
    name: PropTypes.string
  }),
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  refdata: PropTypes.arrayOf(PropTypes.object),
  templates: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    id: PropTypes.string
  }))
};

export default EditSettingValue;
