import React from 'react';

import RenderSettingValue from './RenderSettingValue';

import { renderWithKintHarness } from '../../../../test/jest/helpers';

jest.mock('../../hooks');

const stringSetting = {
  id: '12345',
  key: 'testSettingKey',
  section: 'testSettingSection',
  settingType: 'String',
  value: 'diku-shared',
};

const stringSettingNoValue = {
  id: '12345',
  key: 'testSettingKey',
  section: 'testSettingSection',
  settingType: 'String',
  defValue: 'diku-shared',
};

const stringSettingNoValueOrDefault = {
  id: '12345',
  key: 'testSettingKey',
  section: 'testSettingSection',
  settingType: 'String'
};

const passwordSetting = {
  id: '12345',
  key: 'testSettingKey',
  section: 'testSettingSection',
  settingType: 'Password',
  value: 'secret password',
};

const passwordSettingNoValue = {
  id: '12345',
  key: 'testSettingKey',
  section: 'testSettingSection',
  settingType: 'Password',
  defValue: 'secret password',
};

const passwordSettingNoValueOrDefault = {
  id: '12345',
  key: 'testSettingKey',
  section: 'testSettingSection',
  settingType: 'Password'
};

const refdata = [
  {
    value: 'test_refdata_value',
    label: 'Test refdata value'
  },
  {
    value: 'other_refdata_value',
    label: 'Other refdata value'
  },
  {
    value: 'final_refdata_value',
    label: 'Final refdata value'
  }
];

const refdataSetting = {
  id: '12345',
  key: 'testSettingKey',
  section: 'testSettingSection',
  settingType: 'Refdata',
  value: 'test_refdata_value',
};

const refdataSettingNoValue = {
  id: '12345',
  key: 'testSettingKey',
  section: 'testSettingSection',
  settingType: 'Refdata',
  defValue: 'test_refdata_value',
};

const refdataSettingNoValueOrDefault = {
  id: '12345',
  key: 'testSettingKey',
  section: 'testSettingSection',
  settingType: 'Refdata'
};

const templates = [
  {
    id: 'abcde',
    name: 'Test template'
  },
  {
    id: '98765',
    name: 'Red herring template'
  },
];

const templateSetting = {
  id: '12345',
  key: 'testSettingKey',
  section: 'testSettingSection',
  settingType: 'Template',
  value: 'abcde',
};

const templateSettingNoValue = {
  id: '12345',
  key: 'testSettingKey',
  section: 'testSettingSection',
  settingType: 'Template',
  defValue: 'abcde',
};

const templateSettingNoValueOrDefault = {
  id: '12345',
  key: 'testSettingKey',
  section: 'testSettingSection',
  settingType: 'Template'
};

describe('RenderSettingValue', () => {
  describe('render string settings', () => {
    describe('render a string setting', () => {
      let renderComponent;
      beforeEach(async () => {
        renderComponent = renderWithKintHarness(
          <RenderSettingValue
            currentSetting={stringSetting}
            input={{
              name: 'test'
            }}
          />
        );
      });

      it('renders the value', () => {
        const { getByText } = renderComponent;
        expect(getByText('diku-shared')).toBeInTheDocument();
      });
    });

    describe('render a string setting without a value', () => {
      let renderComponent;
      beforeEach(async () => {
        renderComponent = renderWithKintHarness(
          <RenderSettingValue
            currentSetting={stringSettingNoValue}
            input={{
              name: 'test'
            }}
          />
        );
      });

      it('renders the default value', () => {
        const { getByText } = renderComponent;
        expect(getByText('[default] diku-shared')).toBeInTheDocument();
      });
    });

    describe('render a string setting without a value or default', () => {
      let renderComponent;
      beforeEach(async () => {
        renderComponent = renderWithKintHarness(
          <RenderSettingValue
            currentSetting={stringSettingNoValueOrDefault}
            input={{
              name: 'test'
            }}
          />
        );
      });

      it('renders the empty message', () => {
        const { getByText } = renderComponent;
        expect(getByText('No current value')).toBeInTheDocument();
      });
    });
  });

  describe('render refdata settings', () => {
    describe('render a refdata setting', () => {
      let renderComponent;
      beforeEach(async () => {
        renderComponent = renderWithKintHarness(
          <RenderSettingValue
            currentSetting={refdataSetting}
            input={{
              name: 'test'
            }}
            refdata={refdata}
          />
        );
      });

      it('renders the value', () => {
        const { getByText } = renderComponent;
        expect(getByText('Test refdata value')).toBeInTheDocument();
      });
    });

    describe('render a refdata setting without a value', () => {
      let renderComponent;
      beforeEach(async () => {
        renderComponent = renderWithKintHarness(
          <RenderSettingValue
            currentSetting={refdataSettingNoValue}
            input={{
              name: 'test'
            }}
            refdata={refdata}
          />
        );
      });

      it('renders the default value', () => {
        const { getByText } = renderComponent;
        expect(getByText('[default] Test refdata value')).toBeInTheDocument();
      });
    });

    describe('render a refdata setting without a value or default', () => {
      let renderComponent;
      beforeEach(async () => {
        renderComponent = renderWithKintHarness(
          <RenderSettingValue
            currentSetting={refdataSettingNoValueOrDefault}
            input={{
              name: 'test'
            }}
            refdata={refdata}
          />
        );
      });

      it('renders the empty message', () => {
        const { getByText } = renderComponent;
        expect(getByText('No current value')).toBeInTheDocument();
      });
    });
  });

  describe('render password settings', () => {
    describe('render a password setting', () => {
      let renderComponent;
      beforeEach(async () => {
        renderComponent = renderWithKintHarness(
          <RenderSettingValue
            currentSetting={passwordSetting}
            input={{
              name: 'test'
            }}
          />
        );
      });

      it('renders the value', () => {
        const { getByText } = renderComponent;
        expect(getByText('********')).toBeInTheDocument();
      });
    });

    describe('render a password setting without a value', () => {
      let renderComponent;
      beforeEach(async () => {
        renderComponent = renderWithKintHarness(
          <RenderSettingValue
            currentSetting={passwordSettingNoValue}
            input={{
              name: 'test'
            }}
          />
        );
      });

      it('renders the default value', () => {
        const { getByText } = renderComponent;
        expect(getByText('[default] ********')).toBeInTheDocument();
      });
    });

    describe('render a password setting without a value or default', () => {
      let renderComponent;
      beforeEach(async () => {
        renderComponent = renderWithKintHarness(
          <RenderSettingValue
            currentSetting={passwordSettingNoValueOrDefault}
            input={{
              name: 'test'
            }}
          />
        );
      });

      it('renders the empty message', () => {
        const { getByText } = renderComponent;
        expect(getByText('No current value')).toBeInTheDocument();
      });
    });
  });

  describe('render template settings', () => {
    describe('render a template setting', () => {
      let renderComponent;
      beforeEach(async () => {
        renderComponent = renderWithKintHarness(
          <RenderSettingValue
            currentSetting={templateSetting}
            input={{
              name: 'test'
            }}
            templates={templates}
          />
        );
      });

      it('renders the value', () => {
        const { getByText } = renderComponent;
        expect(getByText('Test template')).toBeInTheDocument();
      });
    });

    describe('render a template setting without a value', () => {
      let renderComponent;
      beforeEach(async () => {
        renderComponent = renderWithKintHarness(
          <RenderSettingValue
            currentSetting={templateSettingNoValue}
            input={{
              name: 'test'
            }}
            templates={templates}
          />
        );
      });

      it('renders the default value', () => {
        const { getByText } = renderComponent;
        expect(getByText('Test template')).toBeInTheDocument(); // TODO this doesn't currently have a [default] marking on it
      });
    });

    describe('render a template setting without a value or default', () => {
      let renderComponent;
      beforeEach(async () => {
        renderComponent = renderWithKintHarness(
          <RenderSettingValue
            currentSetting={templateSettingNoValueOrDefault}
            input={{
              name: 'test'
            }}
            templates={templates}
          />
        );
      });

      it('renders the empty message', () => {
        const { getByText } = renderComponent;
        expect(getByText('No current value')).toBeInTheDocument();
      });
    });
  });
});
