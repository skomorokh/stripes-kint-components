import { forwardRef, useImperativeHandle } from 'react';
import PropTypes from 'prop-types';

import classnames from 'classnames';

import {
  IconButton,
  Icon,
} from '@folio/stripes/components';

import css from '../../../styles/IconSelect.css';

import RichSelect, { useSelectedOption } from '../RichSelect';
import FormattedKintMessage from '../FormattedKintMessage';

// A form component which acts as a "Select for states with corresponding icons"
const IconSelect = forwardRef(({
  ariaLabel,
  disabled = false,
  id,
  input,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  label,
  meta,
  notSet,
  onChange,
  options: userOptions = [],
  required = false,
  value
}, ref) => {
  const defaultNotSet = {
    icon: 'ellipsis',
    value: '',
    label: <FormattedKintMessage
      id="notSet"
      intlKey={passedIntlKey}
      intlNS={passedIntlNS}
    />
  };

  // Options with notSet
  const options = [
    notSet ?? defaultNotSet,
    ...userOptions
  ];

  const [richSelectRef, selectedOption] = useSelectedOption();
  useImperativeHandle(ref, () => ({
    selectedOption
  }));

  const { className: iconButtonClassName, ...buttonProps } = selectedOption?.buttonProps ?? {};
  return (
    <RichSelect
      ref={richSelectRef}
      ariaLabel={ariaLabel}
      disabled={disabled}
      id={id}
      input={input}
      label={label}
      meta={meta}
      onChange={onChange}
      options={options}
      renderOption={(opt) => {
        if (opt.icon) {
          return (
            <Icon
              icon={opt.icon}
              {...opt.iconProps}
            >
              {opt.label ?? opt.value}
            </Icon>
          );
        }

        return opt.label ?? opt.value;
      }}
      renderTrigger={({ onToggle, triggerRef, keyHandler, ariaProps, getTriggerProps }) => {
        return (
          <IconButton
            ref={triggerRef}
            disabled={disabled}
            icon={selectedOption?.icon ?? 'ellipsis'}
            marginBottom0
            onClick={onToggle}
            onKeyDown={keyHandler}
            type="button"
            {...{
              className: classnames(
                css.buttonStyle,
                iconButtonClassName
              ),
              ...buttonProps
            }}
            {...getTriggerProps()}
            {...ariaProps}
          />
        );
      }}
      required={required}
      value={value}
    />
  );
});

IconSelect.propTypes = {
  ariaLabel: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  input: PropTypes.object,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.func,
    PropTypes.node
  ]),
  meta: PropTypes.object,
  notSet: PropTypes.shape({
    icon: PropTypes.string,
    value: PropTypes.string.isRequired,
    label: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.node
    ])
  }),
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.shape({
    icon: PropTypes.string,
    value: PropTypes.string.isRequired,
    label: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.node
    ])
  })),
  required: PropTypes.bool,
  value: PropTypes.string,
};

export default IconSelect;
