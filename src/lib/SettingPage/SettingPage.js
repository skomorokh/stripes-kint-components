import React, { useContext } from 'react';
import PropTypes from 'prop-types';

import { EditableSettingsList } from '../EditableSettingsList';
import { SettingsContext } from '../contexts';
// Importing directly here to avoid cyclic dependency
import useSettingSection from '../settingsHooks/useSettingSection';

const SettingPage = ({
  allowEdit = true,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  sectionName
}) => {
  const { settingEndpoint } = useContext(SettingsContext);

  const { handleSubmit, settings } = useSettingSection({
    sectionName,
    settingEndpoint
  });

  return (
    <EditableSettingsList
      allowEdit={allowEdit}
      initialValues={{ 'settings': settings }}
      intlKey={passedIntlKey}
      intlNS={passedIntlNS}
      labelOverrides={labelOverrides}
      onSave={handleSubmit}
      onSubmit={handleSubmit}
      settingSection={sectionName}
    />
  );
};

SettingPage.propTypes = {
  allowEdit: PropTypes.bool,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  sectionName: PropTypes.string,
};

export default SettingPage;
