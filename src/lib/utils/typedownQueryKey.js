const typedownQueryKey = (path) => {
  const keyArray = ['stripes-kint-components', 'typedown'];
  if (path) {
    keyArray.push(path);
  }
  return keyArray;
};

export default typedownQueryKey;
