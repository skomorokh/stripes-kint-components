const sortByLabel = (a, b) => {
  const al = a.label?.toLowerCase() ?? a.id?.toLowerCase();
  const bl = b.label?.toLowerCase() ?? b.id?.toLowerCase();
  if (al < bl) {
    return -1;
  }

  if (al > bl) {
    return 1;
  }

  return 0;
};

export default sortByLabel;
