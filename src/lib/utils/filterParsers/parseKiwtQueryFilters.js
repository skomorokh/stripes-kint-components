import parseKiwtQueryGroups from './parseKiwtQueryGroups';
import parseKiwtQueryString from './parseKiwtQueryString';

// This is an amalgam of parseKiwtQueryGroups, which splits up grouped queries
// and parseKiwtQueryString, which figures out comparator vs attribute, etc

// This will handle the nested recursion logic, switching between String and Array
export const parseKiwtQueryFiltersRecursive = (query) => {
  if (Array.isArray(query)) {
    return query.map(q => parseKiwtQueryFiltersRecursive(q));
  } else if (typeof query === 'string') {
    if (query === '&&' || query === '||') {
      return query;
    } else {
      return parseKiwtQueryString(query);
    }
  } else {
    throw new Error(`Unexpected query type: ${query}`);
  }
};

// This accepts a STRING only
const parseKiwtQueryFilters = (queryString) => {
  if (typeof queryString === 'string') {
    // parseKiwtQueryGroups already recurses, so we know is the right shape all the way down
    const queryGroups = parseKiwtQueryGroups(queryString);

    return parseKiwtQueryFiltersRecursive(queryGroups);
  } else {
    throw new Error(`parseKiwtQuery expects a parameter of type String, passed: ${queryString}`);
  }
};

/* RETURN SHAPE
 * This will return nested arrays of Objects, Strings and Arrays.
 * Each array refers to a bracketed group
 * Each object is a single query value (attribute, comparator and value)
 * Each string should be boolean && or ||
 * To be read from left to right
 */

export default parseKiwtQueryFilters;
