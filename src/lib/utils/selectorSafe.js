const selectorSafe = (string) => {
  // Normalise to separate diacritics from their base characters as "marks"
  // then strip out all marks.
  return string.normalize('NFKD')?.replace(/\p{M}/gu, '')
    // Then swap out any non-letter/number characters (Also ignore - and _) for `_`
    ?.replace(/[^\p{L}\p{N}\-_]/gu, '_');
};

export default selectorSafe;

/*
 * Should transform as follows:
 * %%&&||()_-*^%$£"!:;@'~#<>/??|\ --> _________-____________________
 * étéaoûtçapère; --> eteaoutcapere_
 * Wörtertschüss; --> wortertschuss_
 * mañanaángel'brødfråłzaźlepięćkuşgöz`tŷ --> mananaangel_brødfrałzazlepieckusgoz_ty
 * æøß --> æøß
 * How do we handle numbers 00!1 --> how_do_we_handle_numbers_00_1
 * This is a normal case --> this_is_a_normal_case
 */
