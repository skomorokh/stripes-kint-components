const parseModConfigEntry = (setting) => {
  let parsedSettings = {};
  if (setting.value) {
    try {
      parsedSettings = JSON.parse(setting.value);
    } catch (error) {
      // Error parsing settings JSON
      console.error(error); // eslint-disable-line no-console
    }
  }

  return parsedSettings;
};

export default parseModConfigEntry;
