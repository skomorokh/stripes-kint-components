const conditionalEncodeURIComponent = (str, encode = true) => {
  if (encode) {
    return encodeURIComponent(str);
  }

  return str;
};

// TODO might be nice to allow for filterOptions to be of the shape of parsedKiwtQueryFilters,
// so we can make use of the deparser there optionally for built in query grouping
const buildFilterOptionBlock = (opf, isNested = false, encode = true) => {
  if (opf?.groupValues) {
    const groupValues = opf.groupValues;

    // Small utility function to add negation and brackets to the options block where necessary
    const negationAndNesting = (str) => `${groupValues?.NOT ? '!' : ''}${(isNested || groupValues?.NOT) ? '(' : ''}${str}${(isNested || groupValues?.NOT) ? ')' : ''}`;

    // First check whether groupValues is ANDed or ORed together
    if (
      (groupValues?.AND && Array.isArray(groupValues.AND)) ||
      (groupValues?.OR && Array.isArray(groupValues.OR))
    ) {
      // AND takes precedence
      if (groupValues.AND) {
        return negationAndNesting(groupValues?.AND?.map(gvo => buildFilterOptionBlock(gvo, true, encode))?.join('&&'));
      }
      return negationAndNesting(groupValues?.OR?.map(gvo => buildFilterOptionBlock(gvo, true, encode))?.join('||'));
    }
    // If neither valid OR nor AND exist, ignore the block
  } else if (opf?.values) {
    // Build the values filter block
    const innerFilters = [];
    opf.values.forEach(opfv => {
      if (opf.path) {
        innerFilters.push(`${opf.path}${opf.comparator ?? '=='}${opfv}`);
      } else {
        innerFilters.push(opfv);
      }
    });

    return conditionalEncodeURIComponent(innerFilters.join('||'), encode);
  } else if (opf?.value) { // If no value OR values, then ignore
    if (opf.path) {
      const filterString = `${opf.path}${opf.comparator ?? '=='}${opf.value}`;
      return conditionalEncodeURIComponent(filterString, encode);
    }

    return conditionalEncodeURIComponent(opf.value, encode);
  }

  return null;
};

const generateKiwtQueryParams = (options, nsValues, encode = true) => {
  const { qindex, query, filters, sort } = nsValues;
  const {
    searchKey = '',
    filterConfig = [],
    /* Assumtion made that if no filterKey is provided then the given filterValues for that key are standalaone, ie require no comparator or key */
    filterKeys = {},
    sortKeys = {},
    stats = true,
    /* Of the form [{ path: 'this.is.some.path', direction: 'asc'/'desc', value: 'someOverrideValue'}, ...]
     * If only path is passed then assume asc.
     * If value is passed then we ignore path/direction and append "sort=${value}" directly
     */
    sort: optionsSort,
    /* Of the form
      [
        {
          path: 'this.is.some.path'
          comparator: '=='
          value: 'this is a value' //OR
          values: ['value1', 'value2'] //OR
          groupValues: { // This is an object containing objects either in groups of AND or OR. AND takes precedence
            AND: [
              // Objects of the same shape as an individual filters object, recursively.
            ],
            NOT: true // When this is set to true, the entire group is negated
          }
        },
        ...
      ]
     * This (with value instead of values) will construct a query param: "filters=this.is.some.path==this is a value"
     * If only value is passed, then it will construct directly: "filters=value"
     * If no comparator is passed, it assumes '=='
     * Values overwrites value and will construct "filters=this.is.some.path==value1||this.is.some.path==value2"
     * Values WITHOUT path will construct "filters=value1||value2"
     *
     * GroupValues will override everything above, and group into brackets.
     *
     * If more complex query building is desired, this should be done externally and passed in as a standalone 'value'
     */
    filters: optionsFilters,
    ...rest
  } = options;

  const paramsArray = [];

  if (query) {
    paramsArray.push(...((qindex || searchKey)?.split(',') ?? []).map(m => `match=${conditionalEncodeURIComponent(m, encode)}`));
    paramsArray.push(`term=${conditionalEncodeURIComponent(query, encode)}`);
  }

  // Actually build the optionsFilters block (Moved logic to its own function to allow recursion)
  if (optionsFilters) {
    optionsFilters.forEach(opf => {
      const optionsBlock = buildFilterOptionBlock(opf, false, encode);
      if (optionsBlock) {
        paramsArray.push(`filters=${optionsBlock}`);
      }
    });
  }

  if (filters) {
    const filterMap = {};
    filters.split(',').forEach(filter => {
      const [filterName, ...filterRest] = filter.trim()?.split('.') ?? [];
      const filterValue = filterRest.join('.');

      if (filterMap[filterName] === undefined) filterMap[filterName] = [];
      filterMap[filterName].push(filterValue);
    });

    // We now have a filterMap of shape { status: ['active', 'cancelled'], type: ['local'] }
    Object.entries(filterMap).forEach(([filterName, filterValues]) => {
      const filterConfigEntry = filterConfig.find(conf => conf.name === filterName);
      const filterKey = filterKeys[filterName];
      if (filterConfigEntry) {
        // We have a direct mapping instruction, use it
        const filterString = filterValues.map(v => {
          const fceValue = filterConfigEntry?.values?.find(fce => fce.name === v)?.value;
          return `${filterName}==${fceValue ?? v}`;
        }).join('||');

        paramsArray.push(`filters=${conditionalEncodeURIComponent(filterString, encode)}`);
      } else if (!filterKey) {
        // These filters have no key mapping so we just pass the values to the backend as-is.
        paramsArray.push(...(filterValues ?? []).map(f => `filters=${conditionalEncodeURIComponent(f, encode)}`));
      } else {
        const filterString = filterValues.map(v => `${filterKey}==${v}`).join('||');
        paramsArray.push(`filters=${conditionalEncodeURIComponent(filterString, encode)}`);
      }
    });
  }

  if (optionsSort && optionsSort.length > 0) {
    optionsSort.forEach(os => {
      if (os.value) {
        paramsArray.push(`sort=${conditionalEncodeURIComponent(os.value, encode)}`);
      } else if (os.path) { // If no path then ignore
        const sortString = `${os.path};${os.direction ?? 'asc'}`;
        paramsArray.push(`sort=${conditionalEncodeURIComponent(sortString, encode)}`);
      }
    });
  }

  if (sort) {
    paramsArray.push(...(sort.trim()?.split(',') ?? []).map(sortKey => {
      const descending = sortKey.startsWith('-');
      let term = sortKey.replace('-', '');

      if (term in sortKeys) {
        term = term.replace(term, sortKeys[term]);
      }

      const sortString = `${term};${descending ? 'desc' : 'asc'}`;

      return `sort=${conditionalEncodeURIComponent(sortString, encode)}`;
    }));
  }

  if (stats) {
    paramsArray.push('stats=true');
  }

  for (const [key, value] of Object.entries(rest)) {
    paramsArray.push(`${key}=${conditionalEncodeURIComponent(value, encode)}`);
  }

  return paramsArray;
};

export default generateKiwtQueryParams;
