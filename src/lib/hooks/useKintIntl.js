import { useIntl } from 'react-intl';
import useIntlKey from './useIntlKey';

/* A hook to enrich the intl object we get from useIntl */
const useKintIntl = (passedIntlKey, passedIntlNS) => {
  const intlObj = useIntl();
  const intlKey = useIntlKey(passedIntlKey, passedIntlNS);

  // FormatKintMessage id misses out the top level key part of the path.
  // This works analogously to FormattedKintMessage component
  const formatKintMessage = ({ id, overrideValue, fallbackMessage, ...formatParams }, formatValues) => {
    if (overrideValue) {
      // Version 3 is a breaking change, where labelOverrides must be strings.
      if (typeof overrideValue !== 'string') {
        throw new Error('Override values must be strings as of stripes-kint-components ^3.0.0');
      }

      if (intlObj.messages?.[overrideValue]) {
        // We've been passed a key as an override, use it
        return intlObj.formatMessage({ id: overrideValue, ...formatParams }, formatValues);
      }
      // At this stage we have an overrideValue that's not a key, return it.
      return overrideValue;
    }

    // If key does not exist in intl, and we have a specified "fallbackMessage", use that

    // (FallbackMessage works like defaultMessage but with no warning)
    if (!intlObj.messages?.[`${intlKey}.${id}`] && fallbackMessage) {
      // Allow fallback message to be an intl key
      if (intlObj.messages?.[fallbackMessage]) {
        return intlObj.formatMessage({ id: fallbackMessage, ...formatParams }, formatValues);
      }

      return fallbackMessage;
    }

    // After all that, render the message from key
    return intlObj.formatMessage({ id: `${intlKey}.${id}`, ...formatParams }, formatValues);
  };

  const messageExists = (key) => !!intlObj.messages?.[`${intlKey}.${key}`];

  return {
    ...intlObj,
    formatKintMessage,
    messageExists
  };
};

export default useKintIntl;
