import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';

import queryString from 'query-string';
import isEqual from 'lodash/isEqual';

let helperObject = {};

const useHelperApp = (helpers) => {
  const history = useHistory();
  const location = useLocation();

  const query = queryString.parse(location.search);

  const [currentHelper, setCurrentHelper] = useState(query?.helper);
  const handleToggleHelper = useCallback((helper) => {
    setCurrentHelper(helper !== currentHelper ? helper : undefined);
  }, [currentHelper]);

  const isOpen = (hlp) => {
    return currentHelper === hlp;
  };

  useEffect(() => {
    // Keep object outside of hook to avoid redraw, oncly change when keys change
    if (!isEqual(Object.keys(helperObject), Object.keys(helpers))) {
      helperObject = helpers;
    }

    if (currentHelper !== query?.helper) {
      const newQuery = {
        ...query,
        helper: currentHelper
      };

      history.push({
        pathname: location.pathname,
        search: `?${queryString.stringify(newQuery)}`
      });
    }
  }, [currentHelper, helpers, history, location, query]);

  // Set the HelperComponent
  const HelperComponent = useMemo(() => ((props) => {
    if (!query?.helper) return null;

    let Component = null;

    Component = helperObject[query?.helper];

    if (!Component) return null;

    return (
      <Component
        onToggle={() => handleToggleHelper(query?.helper)}
        {...props}
      />
    );
  }), [handleToggleHelper, query.helper]);

  // Set up the helperToggleFunctions
  const helperToggleFunctions = {};
  Object.keys(helperObject).forEach(h => {
    helperToggleFunctions[h] = () => handleToggleHelper(h);
  });

  return { currentHelper, HelperComponent, helperToggleFunctions, isOpen };
};

export default useHelperApp;
