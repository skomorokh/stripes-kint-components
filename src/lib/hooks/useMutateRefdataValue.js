import { useMutation } from 'react-query';

import { useOkapiKy } from '@folio/stripes/core';

import useInvalidateRefdata from './useInvalidateRefdata';

const useMutateRefdataValue = ({
  afterQueryCalls,
  catchQueryCalls,
  endpoint,
  id,
  queryParams,
  returnQueryObject = {
    put: false,
    delete: false
  }
}) => {
  const returnObj = {};

  const ky = useOkapiKy();
  const invalidateRefdata = useInvalidateRefdata();

  const deleteQueryObject = useMutation(
    ['stripes-kint-components', 'useMutateRefdataValue', 'delete', id],
    async (data) => ky.put(
      `${endpoint}/${id}`,
      {
        json: {
          id,
          values: [
            {
              id: data,
              _delete: true
            }
          ]
        }
      }
    ).json()
      .then(afterQueryCalls?.delete)
      .then(() => invalidateRefdata())
      .catch(catchQueryCalls?.delete),
    queryParams?.delete
  );

  const putQueryObject = useMutation(
    ['stripes-kint-components', 'editableRefdataList', 'editValue', id],
    async (data) => ky.put(
      `${endpoint}/${id}`,
      {
        json: {
          id,
          values: [
            data
          ]
        }
      }
    ).json()
      .then(afterQueryCalls?.put)
      .then(() => invalidateRefdata())
      .catch(catchQueryCalls?.put),
    queryParams?.put
  );

  if (returnQueryObject?.delete) {
    returnObj.delete = deleteQueryObject;
  } else {
    returnObj.delete = deleteQueryObject.mutateAsync;
  }

  if (returnQueryObject?.put) {
    returnObj.put = putQueryObject;
  } else {
    returnObj.put = putQueryObject.mutateAsync;
  }

  return returnObj;
};

export default useMutateRefdataValue;
